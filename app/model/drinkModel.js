//Import thư viện mongoose
const mongoose = require("mongoose");

//Class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//Khởi tạo instance drinkSchema từ Class Schema
const drinkSchema = new Schema({
	maNuocUong: {
        type: String, 
        unique: true, 
        required: false
    },
	tenNuocUong: {
        type: String, 
        required: false
    },
	donGia: {
        type:Number, 
        required: false
    }
});

// Biên dịch drink Model từ drinkSchema
module.exports = mongoose.model("Drink", drinkSchema);